package ru.shumov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Session;

import java.util.Collection;
import java.util.List;

public interface SessionRepository {

    List<Session> findAll();

    Session findOne(@NotNull String id);

    void persist(@NotNull Session session);

    void merge(@NotNull Session session);

    void remove(@NotNull String id);

    void removeAll();
}
