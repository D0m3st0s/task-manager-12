#### Требования к Software
- JRE 18
#### Стек Технологий
- Java SE 18
- Maven
#### Контактные данные разработчика
- bahrsshumov@gmail.com
#### Команды для сборки
```
mvn clean install
```
#### Команды для запуска приложения
```
java -jar ./<.jar file>
```