package ru.shumov.tm.entity;

import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import ru.shumov.tm.enums.Status;


import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@XmlRootElement
@XmlAccessorType
public class Project implements Serializable {
    private String id;
    private String name;
    private String description;
    private Date creatingDate;
    private Date startDate;
    private Date endDate;
    private String UserId;
    private Status status = Status.PLANNED;
}
