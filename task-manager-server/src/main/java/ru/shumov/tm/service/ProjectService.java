package ru.shumov.tm.service;

import ru.shumov.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface ProjectService {

    void create(Project project);

    void update(Project project);

    void clear();

    void remove(String projectId, String userId);

    Collection<Project> getList();

    List<Project> getSortedList(String id, String method);

    List<Project> find(String id, String part);

    List<Project> getList(String id);

    Project getOne(String id, String userID);

    boolean checkKey(String id, String userID);
}