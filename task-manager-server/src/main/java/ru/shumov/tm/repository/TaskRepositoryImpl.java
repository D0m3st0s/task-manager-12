package ru.shumov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.enums.Status;
import ru.shumov.tm.service.Connector;

import java.sql.*;
import java.sql.Date;
import java.util.*;

public class TaskRepositoryImpl implements TaskRepository {
    private final int ID_FIELD = 1;
    private final int USER_ID_FIELD = 2;
    private final int PROJECT_ID_FIELD = 3;
    private final int DESCRIPTION_FIELD = 4;
    private final int STATUS_FIELD = 5;
    private final int CREATING_DATE_FIELD = 6;
    private final int START_DATE_FIELD = 7;
    private final int END_DATE_FIELD = 8;
    private final int NAME_FIELD = 9;
    private final Connection connection = Connector.connectionDB();
    @SneakyThrows
    public Collection<Task> findAll() {
        final List<Task> tasks = new ArrayList<>();
        final  String query = "SELECT * FROM tasks";
        final Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            tasks.add(fetch(resultSet));
        }
        statement.close();
        return tasks;
    }
    @SneakyThrows
    public List<Task> findAll(@NotNull String id) {
        final List<Task> tasks = new ArrayList<>();
        final  String query = "SELECT * FROM tasks WHERE user_id= ?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            tasks.add(fetch(resultSet));
        }
        statement.close();
        return tasks;
    }
    @SneakyThrows
    public Task findOne(@NotNull String id, String userId) {
        final  String query = "SELECT * FROM tasks WHERE id=? AND user_id=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final Task task = fetch(resultSet);
        statement.close();
        if(task == null) return null;
        else return task;
    }
    @SneakyThrows
    public void persist(@NotNull Task task) {
        final String query = "INSERT INTO tasks VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setString(2, task.getUserId());
        statement.setString(3, task.getProjectId());
        statement.setString(4, task.getDescription());
        statement.setString(5, task.getStatus().toString());
        statement.setDate(6, new Date(task.getCreatingDate().getTime()));
        statement.setDate(7, new Date(task.getStartDate().getTime()));
        statement.setDate(8, new Date(task.getEndDate().getTime()));
        statement.setString(9, task.getName());
        statement.executeUpdate();
        statement.close();
    }
    @SneakyThrows
    public void merge(@NotNull Task task) {
        final String query = "UPDATE tasks SET user_id=?, project_id=?, description=?, " +
                "status=?, creating_date=?, start_date=?, end_date=?, name=?, WHERE id=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getUserId());
        statement.setString(2, task.getProjectId());
        statement.setString(3, task.getDescription());
        statement.setString(4, task.getStatus().toString());
        statement.setDate(5, new Date(task.getCreatingDate().getTime()));
        statement.setDate(6, new Date(task.getStartDate().getTime()));
        statement.setDate(7, new Date(task.getEndDate().getTime()));
        statement.setString(8, task.getName());
        statement.setString(9, task.getId());
        statement.executeUpdate();
        statement.close();
    }
    @SneakyThrows
    public void remove(@NotNull String id) {
        final String query = "DELETE FROM tasks WHERE id=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }
    @SneakyThrows
    public void removeAll() {
        final String query = "DELETE FROM tasks";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }
    @SneakyThrows
    private Task fetch(final ResultSet row) {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString(ID_FIELD));
        task.setUserId(row.getString(USER_ID_FIELD));
        task.setDescription(row.getString(DESCRIPTION_FIELD));
        task.setCreatingDate(row.getDate(CREATING_DATE_FIELD));
        task.setStartDate(row.getDate(START_DATE_FIELD));
        task.setEndDate(row.getDate(END_DATE_FIELD));
        task.setName(row.getString(NAME_FIELD));
        task.setProjectId(row.getString(PROJECT_ID_FIELD));
        task.setStatus(Status.valueOf(row.getString(STATUS_FIELD)));
        return task;
    }

}
