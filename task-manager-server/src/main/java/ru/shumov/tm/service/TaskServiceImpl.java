package ru.shumov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.exceptions.IncorrectInputException;
import ru.shumov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class TaskServiceImpl implements TaskService {
    private TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void create(@NotNull Task task) {
        try {
            if (task.getName() == null || task.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "task name");
            }
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "project id");
            }
            taskRepository.persist(task);
        }
        catch (IncorrectInputException incorrectInput) {
            incorrectInput.printStackTrace();
        }
    }

    public void clear() {
        taskRepository.removeAll();
    }

    public void remove(@NotNull String taskId) {
        taskRepository.remove(taskId);
    }

    public Collection<Task> getList() {
        return taskRepository.findAll();
    }

    public List<Task> getList(@NotNull String id) {return taskRepository.findAll(id);}

    public List<Task> getSortedList(String id, String method) {
        List<Task> tasks = taskRepository.findAll(id);
        sorting(method, tasks);
        return tasks;
    }

    public Task getOne(@NotNull String id, String userId) {
        return taskRepository.findOne(id, userId);
    }

    public List<Task> find(String id, String part) {
        List<Task> values = taskRepository.findAll(id);
        List<Task> tasks = new ArrayList<>();
        for (Task task : values) {
            if(task.getName().contains(part) || task.getDescription().contains(part)) {
                tasks.add(task);
            }
        }
        return tasks;
    }

    public void update(@NotNull Task task) {
        try {
            if (task.getName() == null || task.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "task name");
            }
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "project id");
            }
            taskRepository.merge(task);
        }
        catch (IncorrectInputException incorrectInput) {
            incorrectInput.printStackTrace();
        }
    }

    public boolean checkKey(@NotNull String id, String userId) {
        if(taskRepository.findOne(id, userId) != null) {
            return taskRepository.findOne(id, userId).getId().equals(id);
        }
        return false;
    }
    public void sorting(@NotNull final String method, @NotNull final List<Task> tasks) {
        switch (method) {
            case "by creating date":
                tasks.sort(Comparator.comparing(Task::getCreatingDate));
                break;
            case "by start date":
                tasks.sort(Comparator.comparing(Task::getStartDate));
                break;
            case "by end date":
                tasks.sort(Comparator.comparing(Task::getEndDate));
                break;
            case "by status":
                tasks.sort(Comparator.comparing(Task::getStatus));
                break;
        }

    }
}
