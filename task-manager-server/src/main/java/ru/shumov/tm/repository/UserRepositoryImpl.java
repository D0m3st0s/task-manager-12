package ru.shumov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.service.Connector;
import ru.shumov.tm.service.Md5ServiceImpl;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public class UserRepositoryImpl implements UserRepository {

    private final int ID_FIELD = 1;
    private final int LOGIN_FIELD = 2;
    private final int PASSWORD_FIELD = 3;
    private final int ROLE_FIELD = 4;
    private final Connection connection = Connector.connectionDB();
    @SneakyThrows
    public void persist(@NotNull User user) {
        final String query = "INSERT INTO users VALUES (?, ?, ?, ?)";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPassword());
        statement.setString(4, user.getRole().toString());
        statement.executeUpdate();
        statement.close();
    }
    @SneakyThrows
    public void merge(@NotNull User user) {
        final String query = "UPDATE users SET login=?, password=?, role=? WHERE id=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getPassword());
        statement.setString(3, user.getRole().toString());
        statement.setString(4, user.getId());
        statement.executeUpdate();
        statement.close();
    }
    @SneakyThrows
    public Collection<User> findAll() {
        final List<User> users = new ArrayList<>();
        final  String query = "SELECT * FROM users";
        final PreparedStatement statement = connection.prepareStatement(query);
        final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            users.add(fetch(resultSet));
        }
        statement.close();
        return users;
    }
    @SneakyThrows
    public User findOne(@NotNull String login) {
        final String query = "SELECT * FROM users WHERE login=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        final ResultSet resultSet = statement.executeQuery();
        final User user = fetch(resultSet);
        statement.close();
        return user;
    }
    @SneakyThrows
    private User fetch(final ResultSet row) {
        if (row == null) return null;
        @NotNull final User user = new User();
        row.next();
        user.setId(row.getString(ID_FIELD));
        user.setLogin(row.getString(LOGIN_FIELD));
        user.setPassword(row.getString(PASSWORD_FIELD));
        final String status = row.getString(ROLE_FIELD).toUpperCase();
        Role role = Role.valueOf(status);
        user.setRole(role);
        return user;
    }

}
