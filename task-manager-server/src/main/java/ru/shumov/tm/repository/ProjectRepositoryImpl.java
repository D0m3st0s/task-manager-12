package ru.shumov.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.enums.Status;
import ru.shumov.tm.service.Connector;

import java.io.File;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class ProjectRepositoryImpl implements ProjectRepository {

    private final int ID_FIELD = 1;
    private final int USER_ID_FIELD = 2;
    private final int NAME_FIELD = 3;
    private final int DESCRIPTION_FIELD = 4;
    private final int STATUS_FIELD = 5;
    private final int CREATING_DATE_FIELD = 6;
    private final int END_DATE_FIELD = 8;
    private final int START_DATE_FIELD = 7;
    @Getter
    private Connection connection = Connector.connectionDB();
    @SneakyThrows
    public List<Project> findAll() {
        final List<Project> projects = new ArrayList<>();
        final  String query = "SELECT * FROM projects";
        final Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            projects.add(fetch(resultSet));
        }
        statement.close();
        return projects;
    }
    @SneakyThrows
    public List<Project> findAll(String id) {
        final List<Project> projects = new ArrayList<>();
        final  String query = "SELECT * FROM projects WHERE user_id= ?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            projects.add(fetch(resultSet));
        }
        statement.close();
        return projects;
    }
    @SneakyThrows
    public Project findOne(@NotNull String id, String userId) {
        final  String query = "SELECT * FROM projects WHERE id=? AND user_id=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final Project project = fetch(resultSet);
        statement.close();
        if(project == null) return null;
        else return project;
    }
    @SneakyThrows
    public void persist(@NotNull String id, @NotNull Project project) {
        final String query = "INSERT INTO projects VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getUserId());
        statement.setString(3, project.getName());
        statement.setString(4, project.getDescription());
        statement.setString(5, project.getStatus().toString());
        statement.setDate(6, new Date(project.getCreatingDate().getTime()));
        statement.setDate(7, new Date(project.getStartDate().getTime()));
        statement.setDate(8, new Date(project.getEndDate().getTime()));
        statement.executeUpdate();
        statement.close();
    }
    @SneakyThrows
    public void merge(@NotNull Project project) {
        final String query = "UPDATE projects SET user_id=?, name=?, description=?, " +
                "status=?, creating_date=?, start_date=?, end_date=? WHERE id=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getUserId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setString(4, project.getStatus().toString());
        statement.setDate(5, new Date(project.getCreatingDate().getTime()));
        statement.setDate(6, new Date(project.getStartDate().getTime()));
        statement.setDate(7, new Date(project.getEndDate().getTime()));
        statement.setString(8, project.getId());
        statement.executeUpdate();
        statement.close();
    }
    @SneakyThrows
    public void remove(@NotNull String id) {
        final String query = "DELETE FROM projects WHERE id=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }
    @SneakyThrows
    public void removeAll() {
        final String query = "DELETE FROM projects";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();

    }
    @SneakyThrows
    public Project fetch(final ResultSet row) {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString(ID_FIELD));
        project.setUserId(row.getString(USER_ID_FIELD));
        project.setDescription(row.getString(DESCRIPTION_FIELD));
        project.setCreatingDate(row.getDate(CREATING_DATE_FIELD));
        project.setStartDate(row.getDate(START_DATE_FIELD));
        project.setEndDate(row.getDate(END_DATE_FIELD));
        project.setName(row.getString(NAME_FIELD));
        final String status = row.getString(STATUS_FIELD).toUpperCase();
        project.setStatus(Status.valueOf(status));
        return project;

    }
}
