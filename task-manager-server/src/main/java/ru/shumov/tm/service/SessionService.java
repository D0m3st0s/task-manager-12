package ru.shumov.tm.service;

import ru.shumov.tm.entity.Session;
import ru.shumov.tm.exceptions.ValidateException;

import java.util.Collection;

public interface SessionService {
    void validate(final Session session) throws ValidateException;

    Session sing(final Session session);

    Session openSession(final String login) throws ValidateException;

    void closeSession(final Session session) throws ValidateException;

    Collection<Session> getSessionList();
}
