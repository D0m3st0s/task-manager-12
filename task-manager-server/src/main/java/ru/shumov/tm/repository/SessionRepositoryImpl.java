package ru.shumov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Session;
import ru.shumov.tm.service.Connector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public class SessionRepositoryImpl implements SessionRepository {

    private final int ID_FIELD = 1;
    private final int TIMESTAMP_FIELD = 2;
    private final int SIGNATURE_FIELD =3;
    private final int USER_ID_FIELD = 4;
    private final int OWNER_ID_FIELD = 5;

    private final Connection connection = Connector.connectionDB();
    @SneakyThrows
    public List<Session> findAll() {
        final String query = "SELECT * FROM sessions";
        final PreparedStatement statement = connection.prepareStatement(query);
        final List<Session> sessions = new ArrayList<>();
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            sessions.add(fetch(resultSet));
        }
        statement.close();
        return sessions;
    }
    @SneakyThrows
    public Session findOne(@NotNull final String id) {
        final String query = "SELECT * FROM sessions WHERE id=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final Session session = fetch(resultSet);
        if(session == null) return null;
        else return session;
    }
    @SneakyThrows
    public void persist(@NotNull final Session session) {
        final String query = "INSERT INTO sessions VALUES (?, ?, ?, ?, ?)";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setLong(2, session.getTimestamp());
        statement.setString(3, session.getSignature());
        statement.setString(4, session.getUserId());
        statement.setString(5, session.getOwnerId());
        statement.executeUpdate();
        statement.close();
    }
    @SneakyThrows
    public void merge(@NotNull final Session session) {
        final String query = "UPDATE sessions SET timestamp=?, signature=?, user_id=?, owner_id=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setLong(1, session.getTimestamp());
        statement.setString(2, session.getSignature());
        statement.setString(3, session.getUserId());
        statement.setString(4, session.getOwnerId());
        statement.setString(5, session.getId());
        statement.executeUpdate();
        statement.close();
    }
     @SneakyThrows
     public void remove(@NotNull final String id) {
        final String query = "DELETE FROM sessions WHERE id=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
     }
     @SneakyThrows
     public void removeAll() {
         final String query = "DELETE FROM sessions";
         final PreparedStatement statement = connection.prepareStatement(query);
         statement.executeUpdate();
         statement.close();
     }
     @SneakyThrows
     public Session fetch(final ResultSet row) {
        if(row == null) return null;
        Session session = new Session();
        row.next();
        session.setId(row.getString(ID_FIELD));
        session.setUserId(row.getString(USER_ID_FIELD));
        session.setTimestamp(row.getLong(TIMESTAMP_FIELD));
        session.setSignature(row.getString(SIGNATURE_FIELD));
        session.setOwnerId(row.getString(OWNER_ID_FIELD));
        return session;
     }
}
