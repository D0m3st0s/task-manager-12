package ru.shumov.tm.service;

import org.jetbrains.annotations.NotNull;

public interface TerminalService {

    String scanner();

    void outPutString(@NotNull String string);

    //void outPutProject(@NotNull Project project);

    //void outPutTask(@NotNull Task task);

    //void outPutUser(@NotNull User user);
}
